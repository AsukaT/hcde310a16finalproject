function initMap(){
	
	var map = new google.maps.Map(document.getElementById('map-canvas'), {
		zoom: 12,
		center: {lat: 47.6, lng: -122.33}
	});
	
    var markers = locations.map(function(location, i) {
    	marker = new google.maps.Marker({
	        position: location
	      });
    	marker.listing = location;
	    marker.addListener('click', function() {
	    	infowindow.setContent(infoText(this.listing));
			infowindow.open(map, this);
		});
      	return marker;
    });
    	    
    var markerCluster = new MarkerClusterer(map, markers,
	        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
 
}
function resizeMap() {
	$("#map-canvas").height($(window).height());
	$("#debugtext").height($(window).height());
	
}
function infoText(listing) {
	htm = '<div id="content">'+
	'<div id="siteNotice">'+ '</div>'+
	'<h1 id="firstHeading" class="firstHeading">' + listing.title + '</h1>'+
	'<div id="bodyContent">'+
	'<img src="' + listing.img + '" style="float:right;">' +
	'<p>Updated:' + listing.published + '</p>' +
	'<p>' + listing.description + '</p>' +
	'<p>Bus routes:' + listing.routes + '</p>' +
    '<p><a href="' + listing.link + '" target="_blank">Trulia listing</a></p>' +
    '</div>'+
    '</div>';
	return htm;
		
}
var infowindow = new google.maps.InfoWindow({
  content: "default"
});
$('#form1').validate({
    rules: {
        in_city: {
            required: true
        },
        in_state: {
            required: true
        }
    }
});

$( document ).on( "pagecreate", "#map-page", function() {
	initMap();
	resizeMap();
});
window.addEventListener("resize", resizeMap);