#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2, urllib2, json, xml.dom.minidom
import jinja2
import os
import logging

#from hcde310a16finalproject/api_keys.py import google_api_key and onebusaway_api_key
google_api_key = 'AIzaSyDtcDgi5lQfAov67Ji6HfPnKH5ci06yDco'
onebusaway_api_key = '3aa2db34-adb8-472a-9fd4-acd81e91a416'

JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class MainHandler(webapp2.RequestHandler):
    def get(self):
        #print statements don't work well
        #print "In MainHandler"
        logging.info("In MainHandler")      

class GreetHandler(webapp2.RequestHandler):
    def get(self):
        vals = {}
        vals['page_title']="Find a walkable house to live"
        template = JINJA_ENVIRONMENT.get_template('finalproject.html')
        self.response.write(template.render(vals))

class Listing():
    def __init__(self, dct):
        self.title = dct['title']
        commasplit = self.title.split(',')
        self.address = ' '.join(commasplit[0:3])
        self.price = ''.join(commasplit[3:]).split(' ')[1]
        self.link = dct['link']
        self.guid = dct['link']   
        self.description = dct['description']
        self.sqft = None
        self.beds = None
        self.baths = None
        for desc in self.description.split(',')[0:3]:
            if 'sqft' in desc:
                self.sqft = float(desc.replace('sqft', ''))
            if 'bed' in desc:
                self.beds = float(desc.replace('beds', '').replace('bed', ''))
            if 'bath' in desc:
                self.baths = float(desc.replace('baths', '').replace('bath', ''))
        
        self.thumbnail = dct['thumbnail']
        self.published = dct['pubdate']
        self.latitude = None
        self.longitude = None
        self.routes = None
    def fillLatLong(self):
        #http://maps.googleapis.com/maps/api/geocode/json?address=title
        loc = getGoogleLatLong(self.address)
        if loc <> None:
            self.latitude = loc['lat']
            self.longitude = loc['lng']
    def fillBusRoutes(self, radiusmeters):
        if self.latitude <> None and self.longitude <> None:
            self.routes = getOneBusAwayRoutes(self.latitude, self.longitude, radiusmeters)
    def checkBusRoutes(self, routedict, radiusmeters):
        self.fillLatLong()
        if self.latitude == None and self.longitude == None:
            print "Lat Lng Error, remove>>"
            return False
        self.fillBusRoutes(radiusmeters)
        if self.routes == None:
            print "Route Error, remove>>"
            return False       

        if len(routedict.keys()) == 0:
            return True
        else:
            listingroutes = self.routes.split(',')
            for route in listingroutes:
                if route in routedict:
                    return True
            return False
        
    def shortDescription(self):
        return "%s (%s, %s)"%(self.title.encode('utf-8'), \
            str(self.latitude), \
            str(self.longitude))
    def __str__(self):
        return "~~~ %s ~~~\naddress: %s\nprice: %s\nguid: %s\nlink: %s\ndescription: %s\nthumbnail: %s\npublished: %s\nlatitude: %s\nlongitude: %s\nbusroutes: %s\nsqft: %s\nbeds: %s\nbaths: %s"%(
              self.title.encode('utf-8'), \
              self.address.encode('utf-8'), \
              self.price.encode('utf-8'), \
              self.guid.encode('utf-8'), \
              self.link.encode('utf-8'), \
              self.description.encode('utf-8'), \
              self.thumbnail.encode('utf-8'), \
              self.published.encode('utf-8'), \
              str(self.latitude), \
              str(self.longitude), \
              self.routes, \
              self.sqft, \
              self.beds, \
              self.baths )

def getOneBusAwayRoutes(latitude, longitude, radiusmeters=None):
    baseurl= "http://api.pugetsound.onebusaway.org/api/where/routes-for-location.json?lat=%s&lon=%s&key=%s" \
        %(latitude, longitude, onebusaway_api_key)
    if (radiusmeters != None):
        baseurl = baseurl + "&radius=%s"%(radiusmeters)
    #print baseurl
    r = safeGet(baseurl)
    routes = None
    if r <> None:
        onebusaway_json_str = r.read()
        #print google_json_str
        j = json.loads(onebusaway_json_str)
        if j['data'] <> None:
            if j['data']['list'] <> None:
                shortnames = [route['shortName'] for route in j['data']['list']]
                routes = ','.join(shortnames)
        # pretty(loc) #['lat']
    return routes

def getGoogleLatLong(address):
    baseurl= "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s"%(urllib2.quote(address), google_api_key)
    #print baseurl
    r = safeGet(baseurl)
    loc = None
    if r <> None:
        google_json_str = r.read()
        #print google_json_str
        j = json.loads(google_json_str)
        if j <> None and j['results'] <> None and len(j['results']) > 0:
            loc = j['results'][0]['geometry']['location']
        # pretty(loc) #['lat']
    return loc

def getTruliaListings(city, state,pricefrom = None, priceto = None, numbeds = None, numbaths = None, sqftfrom = None, sqftto = None):
    baseurl= "https://www.trulia.com/rss2/for_sale/%s,%s" \
        %(city, state)
 
    if (numbeds != None):
        baseurl = baseurl + "/%sp_beds"%(numbeds)
 
    if (numbaths != None):
        baseurl = baseurl + "/%sp_baths"%(numbaths)
    
    if (sqftfrom != None and sqftto != None):
        baseurl = baseurl + "/%s-%s_sqft"%(sqftfrom, sqftto)

    if (pricefrom != None and priceto != None):
        baseurl = baseurl + "/%s-%s_price"%(pricefrom, priceto)
    print baseurl
    r = safeGet(baseurl)
    trulia_rss_str = r.read()
    trulia_data = xml.dom.minidom.parseString(trulia_rss_str)
    items = trulia_data.getElementsByTagName('item')
    def xmlSubnodeValue(item, tagname):
        ret = item.getElementsByTagName(tagname)[0].firstChild.nodeValue
        #print '%s:%s'%(tagname, ret)
        return ret
    listings = [Listing({ \
                         'title':xmlSubnodeValue(item, 'title'), \
                         'guid':xmlSubnodeValue(item, 'guid'), \
                         'link':xmlSubnodeValue(item, 'link'), \
                         'description':xmlSubnodeValue(item, 'description'), \
                         'thumbnail':"http:" + item.getElementsByTagName('media:thumbnail')[0].getAttribute('url'), \
                         'pubdate':xmlSubnodeValue(item, 'pubDate') \
                         }) for item in items]
    return listings

def pretty(obj):
    return json.dumps(obj, sort_keys=True, indent=2)

class GreetResponseHandler(webapp2.RequestHandler):
    def post(self):
        vals = {}
        
        city = self.request.get('in_city')
        vals['in_city'] = city
        
        state = self.request.get('in_state')
        vals['in_state'] = state
        
        numbeds = self.request.get('in_beds')
        vals['in_beds'] = numbeds
        if numbeds == "":
            numbeds = None
        numbaths = self.request.get('in_baths')
        vals['in_baths'] = numbaths
        if numbaths == "":
            numbaths = None
        pricefrom = self.request.get('in_pricefrom')
        vals['in_pricefrom'] = pricefrom
        if pricefrom == "":
            pricefrom = None
        priceto = self.request.get('in_priceto')
        vals['in_priceto'] = priceto
        if priceto == "":
            priceto = None
        sqftfrom = self.request.get('in_sqftfrom')
        vals['in_sqftfrom'] = sqftfrom
        if sqftfrom == "":
            sqftfrom = None
        sqftto = self.request.get('in_sqftto')
        vals['in_sqftto'] = sqftto
        if sqftto == "":
            sqftto = None    
        busroutes = self.request.get('in_routes')
        vals['in_routes'] = busroutes;
        if busroutes == "":
            busroutes = {}
        else:
            tmproutes = busroutes.split(' ,')  
            busroutes = {}
            for route in tmproutes:
                busroutes[route] = True             
        walkminutes = self.request.get('in_walkminutes')
        vals['in_walkminutes'] = walkminutes;
        if walkminutes == "":
            walkmeters = None
        else:
            walkmeters = float(walkminutes) * 83 # 83 meters / minute
        
        truliaResult = getTruliaListings(city, state, pricefrom, priceto, numbeds, numbaths, sqftfrom, sqftto)
        
        #truliaResult = truliaResult[0:35]
        
        listingLength = len(truliaResult)
        listingCount = 0
        for listing in reversed(truliaResult):
            listingCount = listingCount + 1
            if listing.checkBusRoutes(busroutes, walkmeters) == False:
                truliaResult.remove(listing)
                print "(%d of %d) REMOVE %s"%(listingCount, listingLength, listing.shortDescription())
            else:
                print "(%d of %d) KEEP %s"%(listingCount, listingLength, listing.shortDescription())
                #print listing
        print "(COMPLETE) %d listings meet requirements"%(len(truliaResult))
        vals['listings'] = truliaResult
    
        template = JINJA_ENVIRONMENT.get_template('finalproject.html')
        self.response.write(template.render(vals).encode('utf-8'))

def safeGet(url):
    try:
        return urllib2.urlopen(url)
    except urllib2.HTTPError, e:
        print "The server couldn't fulfill the request." 
        print "Error code: ", e.code
    except urllib2.URLError, e:
        print "We failed to reach a server"
        print "Reason: ", e.reason
    return None

# for all URLs except alt.html, use MainHandler
application = webapp2.WSGIApplication([ \
                                      ('/gresponse', GreetResponseHandler),
                                      ('/.*', GreetHandler)
                                      ],
                                     debug=True)
